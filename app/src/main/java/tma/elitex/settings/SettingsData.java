package tma.elitex.settings;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by KrumIliev on 09-Oct-16.
 */

public class SettingsData {

    private final String SERVER_ADDRESS = "server_address";
    private final String PASSWORD = "password";

    private final String defaultServerAddress = "http://178.62.112.203/";
    private final String defaultPassword = "elitex";

    private final String PREFS_NAME = "elitexSettingsPrefs"; // Preference file name
    private SharedPreferences mData;

    public SettingsData(Context context) {
        mData = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
    }

    public void setServerAddress(String newAddress) {
        SharedPreferences.Editor dataEditor = mData.edit();
        dataEditor.putString(SERVER_ADDRESS, newAddress);
        dataEditor.apply();
    }

    public String getServerAddress() {
        return mData.getString(SERVER_ADDRESS, defaultServerAddress);
    }

    public void setPassword(String newPassword) {
        SharedPreferences.Editor dataEditor = mData.edit();
        dataEditor.putString(PASSWORD, newPassword);
        dataEditor.apply();
    }

    public String getPassword() {
        return mData.getString(PASSWORD, defaultPassword);
    }
}
