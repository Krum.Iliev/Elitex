package tma.elitex.settings;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import tma.elitex.R;
import tma.elitex.utils.ExitDialog;

public class SettingsActivity extends AppCompatActivity implements View.OnClickListener, ChangeServerDialog.ChangeServerListener {

    private TextView serverAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        serverAddress = (TextView) findViewById(R.id.server_address);
        serverAddress.setText(new SettingsData(this).getServerAddress());

        findViewById(R.id.server_address_container).setOnClickListener(this);
        findViewById(R.id.password).setOnClickListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.back, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_back:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.server_address_container:
                new ChangeServerDialog(this, this).show();
                break;

            case R.id.password:
                new ChangePasswordDialog(this).show();
                break;
        }
    }

    @Override
    public void onServerAddressChanged(String newAddress) {
        serverAddress.setText(newAddress);
    }
}
