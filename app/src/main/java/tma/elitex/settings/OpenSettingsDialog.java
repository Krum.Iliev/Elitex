package tma.elitex.settings;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.Toast;

import tma.elitex.R;

/**
 * Created by KrumIliev on 09-Oct-16.
 */

public class OpenSettingsDialog extends Dialog implements View.OnClickListener {

    private EditText passInput;

    public OpenSettingsDialog(Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_open_settings);

        passInput = (EditText) findViewById(R.id.password);

        findViewById(R.id.back).setOnClickListener(this);
        findViewById(R.id.open).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                dismiss();
                break;

            case R.id.open:
                open();
                break;
        }
    }

    private void open() {
        String pass = passInput.getText().toString();
        if (pass.equalsIgnoreCase(new SettingsData(getContext()).getPassword())) {
            getContext().startActivity(new Intent(getContext(), SettingsActivity.class));
            dismiss();

        } else {
            Toast.makeText(getContext(), R.string.settings_open_error, Toast.LENGTH_LONG).show();
        }
    }
}
