package tma.elitex.settings;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.Toast;

import tma.elitex.R;

/**
 * Created by KrumIliev on 09-Oct-16.
 */

public class ChangePasswordDialog extends Dialog implements View.OnClickListener {

    private EditText firstPass;
    private EditText secondPass;

    public ChangePasswordDialog(Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_password);

        firstPass = (EditText) findViewById(R.id.first_pass);
        secondPass = (EditText) findViewById(R.id.second_pass);

        findViewById(R.id.back).setOnClickListener(this);
        findViewById(R.id.save).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                dismiss();
                break;

            case R.id.save:
                save();
                break;
        }
    }

    private void save() {
        String fPass = firstPass.getText().toString();
        String sPass = secondPass.getText().toString();

        fPass = fPass.replaceAll("\\s+", "");
        sPass = sPass.replaceAll("\\s+", "");

        if (fPass.isEmpty() || sPass.isEmpty()) {
            Toast.makeText(getContext(), R.string.settings_message_pass_empty, Toast.LENGTH_LONG).show();
            return;
        }

        if (!fPass.equalsIgnoreCase(sPass)) {
            Toast.makeText(getContext(), R.string.settings_message_pass_dont_match, Toast.LENGTH_LONG).show();
            return;
        }

        new SettingsData(getContext()).setPassword(fPass);
        dismiss();
    }
}
