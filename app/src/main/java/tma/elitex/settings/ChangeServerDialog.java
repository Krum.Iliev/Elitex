package tma.elitex.settings;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.Toast;

import tma.elitex.R;

/**
 * Created by KrumIliev on 09-Oct-16.
 */

public class ChangeServerDialog extends Dialog implements View.OnClickListener {

    private EditText serverInput;
    private ChangeServerListener listener;

    public ChangeServerDialog(Context context, ChangeServerListener listener) {
        super(context);
        this.listener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_server_address);

        serverInput = (EditText) findViewById(R.id.server_address);
        serverInput.setText(new SettingsData(getContext()).getServerAddress());

        findViewById(R.id.back).setOnClickListener(this);
        findViewById(R.id.save).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                dismiss();
                break;

            case R.id.save:
                save();
                break;
        }
    }

    private void save() {
        String newAddress = serverInput.getText().toString();
        newAddress = newAddress.replaceAll("\\s+", "");

        if (newAddress.isEmpty()) {
            Toast.makeText(getContext(), R.string.settings_message_server_empty, Toast.LENGTH_LONG).show();
            return;
        }

        if (!newAddress.endsWith("/")) {
            newAddress = newAddress + "/";
        }

        new SettingsData(getContext()).setServerAddress(newAddress);
        listener.onServerAddressChanged(newAddress);
        dismiss();
    }

    interface ChangeServerListener {
        void onServerAddressChanged(String newAddress);
    }
}
