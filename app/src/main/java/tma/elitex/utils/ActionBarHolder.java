package tma.elitex.utils;

import android.content.Context;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import tma.elitex.R;

/**
 * Created by Krum on 5/15/2017.
 */
public class ActionBarHolder {

    private TextView battery;

    public ActionBarHolder(Context context, String title, ActionBar bar) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.actionbar, null);

        battery = (TextView) layout.findViewById(R.id.battery);
        ((TextView) layout.findViewById(R.id.title)).setText(title);
        bar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        bar.setCustomView(layout);
    }

    public void setBatteryLevel(int level) {
        battery.setText(String.valueOf(level) + "%");
    }
}
